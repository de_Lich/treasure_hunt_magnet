using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Controller : MonoBehaviour
{
    #region Singleton Init

    private static UI_Controller _instance;
    private static bool isInitialized = false; // A bit faster singleton

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static UI_Controller Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<UI_Controller>();
        if (_instance != null)
        {
            //_instance.Initialize();
            isInitialized = true;
        }
    }
    #endregion

    [SerializeField] private ItemStatusText itemLostText = null;
    [SerializeField] private ItemStatusText itemDroppedText = null;
    [SerializeField] private ItemStatusText itemSoldText = null;

    [SerializeField] private GameObject itemTradePanel = null;
    [SerializeField] private GameObject resetPanel = null;
    [SerializeField] private GameObject clickBar = null;

    [SerializeField] private ThrowHint throwHint = null;

    private bool isLastItemGood;

    public void ToggleThrowHint(bool value)
    {
        if (value)
        {
            throwHint.Enable();
        }
        else
        {
            throwHint.Disable();
        }
    }

    public void AddClickbarProgress()
    {
        clickBar.GetComponent<ClickbarBehaviour>().AddProgress();
    }

    public void ToggleResetPanel(bool value)
    {
        resetPanel.SetActive(value);
    }

    public void ToggleTradePanel(bool value)
    {
        itemTradePanel.SetActive(value);
    }

    public void ToggleClickbar(bool value)
    {
        clickBar.SetActive(value);
    }

    public void ToggleClickbar(bool value, float addedProgressValue)
    {
        clickBar.SetActive(value);
        clickBar.GetComponent<ClickbarBehaviour>().SetAddedProgressAmount(addedProgressValue);
    }

    public void ToggleText(float duration, UI_TextType textType)
    {
        ItemStatusText targetText = GetItemStatusText(textType);
        targetText.SetStatus(isLastItemGood);
        StartCoroutine(ToggleTextRoutine(duration, targetText));
    }

    private ItemStatusText GetItemStatusText(UI_TextType textType)
    {
        return textType switch
        {
            UI_TextType.ItemLost => itemLostText,
            UI_TextType.ItemDropped => itemDroppedText,
            UI_TextType.ItemSold => itemSoldText,
            _ => null,
        };
    }

/*    private IEnumerator ToggleObjectRoutine(float delay, GameObject gameObject)
    {
        gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
    }*/

    private IEnumerator ToggleTextRoutine(float delay, ItemStatusText text)
    {
        text.Enable();
        yield return new WaitForSeconds(delay);
        text.Disable();
    }
}
