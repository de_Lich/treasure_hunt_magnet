using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowButton : UI_ButtonBase
{
    public override void ButtonAction()
    {
        GamemodeManager.Instance.EnableFinishScreenMode();
        CoroutineActions.ExecuteAction(.85f, () => AnimationController.Instance.PlayDefeat());
        UI_Controller.Instance.ToggleTradePanel(false);
    }
}
