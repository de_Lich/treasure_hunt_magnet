using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellButton : UI_ButtonBase
{
    public override void ButtonAction()
    {
        GamemodeManager.Instance.EnableFinishScreenMode();
        CoroutineActions.ExecuteAction(.85f, () => AnimationController.Instance.PlayVictory());
        VFX_Controller.Instance.PlayConfettiEffect();
        UI_Controller.Instance.ToggleTradePanel(false);
    }
}
