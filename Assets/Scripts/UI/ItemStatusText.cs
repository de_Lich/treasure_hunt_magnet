using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemStatusText : MonoBehaviour
{
    [SerializeField] private string goodItemStatus = default;
    [SerializeField] private string badItemStatus = default;
    private Text text;

    private void Awake()
    {
        text = GetComponent<Text>();
    }

    public void SetStatus(bool isGood)
    {
        if (isGood)
        {
            text.text = goodItemStatus;
        }
        else
        {
            text.text = badItemStatus;
        }
    }

    public void Disable()
    {
        text.enabled = false;
    }

    public void Enable()
    {
        text.enabled = true;
    }
}
