using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetButton : UI_ButtonBase
{
    private MagnetMovementController magnetMovementController = null;
    private MagnetMovementController MagnetMovementController
    {
        get
        {
            if (magnetMovementController == null)
            {
                magnetMovementController = FindObjectOfType<MagnetMovementController>();
            }

            return magnetMovementController;
        }
    }

    public override void ButtonAction()
    {
        UI_Controller.Instance.ToggleResetPanel(false);
        MagnetMovementController.ResetState();
    }
}
