using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickButton : UI_ButtonBase
{
    [SerializeField] private int gainedAmount = 15;

    private MagnetMovementController magnetMovementController;
    private MagnetMovementController MagnetMovementController
    {
        get
        {
            if (magnetMovementController == null)
            {
                magnetMovementController = FindObjectOfType<MagnetMovementController>();
            }

            return magnetMovementController;
        }
    }

    public override void ButtonAction()
    {
        UI_Controller.Instance.AddClickbarProgress();
        MagnetMovementController.TickLerp();
    }
}
