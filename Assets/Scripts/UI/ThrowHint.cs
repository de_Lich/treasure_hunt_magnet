using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ThrowHint : MonoBehaviour
{
    private Image image;

    private void Awake()
    {
        image = GetComponent<Image>();
    }

    private void Update()
    {
        if (Input.touchCount > 0)
        {
            image.rectTransform.position = Input.GetTouch(0).position;
        }
    }

    public void Disable()
    {
        image.enabled = false;
    }

    public void Enable()
    {
        image.enabled = true;
    }
}
