using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartButton : UI_ButtonBase
{
    public override void ButtonAction()
    {
        GameController.Instance.StartGameSession();
        GamemodeManager.Instance.EnableStartScreenMode();
    }
}
