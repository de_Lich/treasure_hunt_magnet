using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UI_TextType
{
    ItemLost,
    ItemDropped,
    ItemSold
}
