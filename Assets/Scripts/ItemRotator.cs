using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemRotator : MonoBehaviour
{
    [SerializeField] private float speed = 1f;
    [SerializeField] private RotationAxis rotationAxis = RotationAxis.Y;

    private float dX, dY, dZ;

    private void Awake()
    {
        ConfigureRotator(rotationAxis);
    }

    private void Update()
    {
        transform.Rotate(dX, dY, dZ);
    }

    public void ConfigureRotator(RotationAxis axis)
    {
        switch (axis)
        {
            case RotationAxis.X:
                dX = speed;
                dY = 0;
                dZ = 0;
                break;
            case RotationAxis.Y:
                dX = 0;
                dY = speed;
                dZ = 0;
                break;
            case RotationAxis.Z:
                dX = 0;
                dY = 0;
                dZ = speed;
                break;
            default:
                break;
        }
    }
}
    public enum RotationAxis
    {
        X, Y, Z
    }