﻿using System.Collections;
using UnityEngine;

public class MagnetMovementController : MonoBehaviour
{
    private const float THROW_DESTINATION_POINT_Y = -6f;

    [SerializeField] private float reelingSpeed = .5f;

    private bool isSwiped = false;
    private bool isUsingClickbar = false;

    // Reeling lerp data
    [SerializeField] private Vector3 lerpDestinationOffset = Vector3.zero;
    [SerializeField] private float lerpSpeed = 1f;
    private float lerpMultiplier = .155f;
    private Vector3 lerpNextPos;
    private Vector3 lerpStartPos;
    private Vector3 lerpTargetPos;

    private Vector3 nextPosition;

    private Touch touch;

    private Vector3 startMagnetPos;
    private float xBound;
    private float zBound;

    // Parabolic movement data
    float stepScale;
    float progress;
    Vector3 throwDestination;

    [SerializeField] private float throwDelay = .45f;

    private void Start()
    {
        GetComponent<MagnetCollisionController>().OnItemCaught += ConfigureLerp;
        startMagnetPos = transform.position;
        lerpTargetPos = lerpTargetPos = startMagnetPos + lerpDestinationOffset;

        Vector3 boundsVector = PlayableAreaController.Instance.MaxBoundsVector;
        xBound = boundsVector.x;
        zBound = boundsVector.z;
    }

    private void Update()
    {
        if (MainData.Instance.gameState == MainData.GameState.PlayingAnimation)
        {
            if (isSwiped)
            {
                CoroutineActions.ExecuteAction(throwDelay, () => ThrowMagnet(throwDestination));
            }
            else
            {
                SetCastingPoint();
            }
        }

        if (MainData.Instance.gameState == MainData.GameState.PlayingGame)
        {
            if (isUsingClickbar)
            {
                AnimationController.Instance.PlayReel();
                LerpMagnet();
            }
            else
            {
                if (Input.touchCount > 0)
                {
                    AnimationController.Instance.PlayReel();
                }
                else
                {
                    AnimationController.Instance.PlayIdle();
                }

                MoveMagnet();
            }
        }
    }

    [NaughtyAttributes.Button]
    public void ResetState()
    {
        LevelManager.Instance.CreateLevel(1, true);
        transform.position = startMagnetPos;
        CoroutineActions.ExecuteAction(.1f, ()=> MainData.Instance.gameState = MainData.GameState.PlayingAnimation);

        lerpNextPos = Vector3.zero;
        lerpStartPos = Vector3.zero;

        isUsingClickbar = false;
        isSwiped = false;

        stepScale = 0f;
        progress = 0f;

        GamemodeManager.Instance.EnableStartScreenMode();
        GetComponent<MagnetCollisionController>().ResetCollisionData();
        AnimationController.Instance.PlayIdleForced();
    }

    private void LerpMagnet()
    {
        if (lerpNextPos != Vector3.zero)
        {
            if (lerpStartPos == Vector3.zero)
            {
                lerpStartPos = transform.position;
            }

            transform.position = Vector3.Lerp(transform.position, lerpNextPos, lerpSpeed * Time.deltaTime);
        }
    }

    public void TickLerp()
    {
        Vector3 delta = lerpNextPos == Vector3.zero
            ? transform.position - lerpTargetPos
            : lerpNextPos - lerpTargetPos;
        lerpNextPos = new Vector3(delta.x * lerpMultiplier, delta.y * lerpMultiplier, -0.5f + delta.z * (1 - 2 * lerpMultiplier));
    }

    private void ConfigureLerp()
    {
        isUsingClickbar = true;
    }

    private void MoveMagnet()
    {
        float zLimit;
        Vector3 gravityToPlayer = Vector3.back / 55;

        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            float touchZ = transform.position.z + (touch.deltaPosition.y * reelingSpeed * Time.deltaTime);
            float touchX = transform.position.x + touch.deltaPosition.x * reelingSpeed * Time.deltaTime;

            zBound = transform.position.z;
            zLimit = zBound;
            if (zBound > zLimit)
            {
                zBound = zLimit;
            }

            touchX = Mathf.Clamp(touchX, -xBound, xBound);
            touchZ = Mathf.Clamp(touchZ, -zBound, zBound);
            nextPosition.Set(touchX, transform.position.y, touchZ);

            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = nextPosition + gravityToPlayer;
            }
        }
    }

    private void SetCastingPoint()
    {
        if (Input.touchCount > 0)
        {
            touch = Input.GetTouch(0);

            Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                if (hit.collider != null)
                {
                    throwDestination = hit.point;
                    throwDestination.y = THROW_DESTINATION_POINT_Y;
                    //Debug.DrawLine(ray.origin, hit.point);
                }
            }

            if (touch.phase == TouchPhase.Began)
            {
                UI_Controller.Instance.ToggleThrowHint(true);
            }

            if (touch.phase == TouchPhase.Ended)
            {
                AnimationController.Instance.PlayThrow();
                UI_Controller.Instance.ToggleThrowHint(false);
                isSwiped = true;
            }
        }
    }

    private void ThrowMagnet(Vector3 targetLocation)
    {
        float throwSpeed = Mathf.Clamp(Mathf.Floor(targetLocation.z), 2.5f, 16);
        float distance = Vector3.Distance(startMagnetPos, targetLocation);

        stepScale = throwSpeed / distance;

        progress = Mathf.Min(progress + Time.deltaTime * stepScale, 1.0f);
        float parabola = 1.0f - 4.0f * (progress - 0.5f) * (progress - 0.5f);
        Vector3 nextPos = Vector3.Lerp(startMagnetPos, targetLocation, progress);
        nextPos.y += parabola * 2;
        transform.position = nextPos;
    }
}