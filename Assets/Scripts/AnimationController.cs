using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationController : MonoBehaviour
{
    [SerializeField] private Animator handsAnimator = null;
    [SerializeField] private Animator characterAnimator = null;

    #region Singleton Init
    private static AnimationController _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static AnimationController Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<AnimationController>();
        //_instance.Initialize();
    }
    #endregion

    public void PlayThrow()
    {
        handsAnimator.Play("throws rope_anim", 0, 0f);
    }

    public void PlayReel()
    {
        handsAnimator.SetBool("IsReeling", true);
    }

    public void PlayIdle()
    {
        handsAnimator.SetBool("IsReeling", false);
        handsAnimator.SetBool("IsThrowing", false);
    }

    public void PlayIdleForced()
    {
        handsAnimator.Play("Hands Idle", 0, 0f);
        handsAnimator.SetBool("IsReeling", false);
        handsAnimator.SetBool("IsThrowing", false);
    }

    public void PlayVictory()
    {
        characterAnimator.Play("Victory", 0, 0f);
    }

    public void PlayDefeat()
    {
        characterAnimator.Play("Defeat", 0, 0f);
    }
}
