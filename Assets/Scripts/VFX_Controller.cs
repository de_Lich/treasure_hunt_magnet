using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFX_Controller : MonoBehaviour
{
    #region Singleton Init
    private static VFX_Controller _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static VFX_Controller Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<VFX_Controller>();
        _instance.Initialize();
    }
    #endregion

    [SerializeField] private GameObject splashParticlePrefab = null;
    [SerializeField] private GameObject confettiParticlePrefab = null;
    [SerializeField] private Transform rotatingPrefabPos = null;
    
    private GameObject splashParticleObject;
    private GameObject confettiParticleObject;
    private GameObject currentRotatingObject;

    private void Initialize()
    {
        splashParticleObject = Instantiate(splashParticlePrefab);
        confettiParticleObject = Instantiate(confettiParticlePrefab);
    }

    public void PlaySplashEffect(Vector3 targetPosition)
    {
        splashParticleObject.SetActive(true);
        splashParticleObject.transform.position = targetPosition;
        splashParticleObject.GetComponent<ParticleSystem>().Play();
    }
    
    [NaughtyAttributes.Button]
    public void PlayConfettiEffect()
    {
        confettiParticleObject.transform.SetParent(Camera.main.transform);
        confettiParticleObject.transform.localRotation = Quaternion.Euler(Vector3.zero);
        confettiParticleObject.transform.localPosition = new Vector3(-0.34f, -1.07f, 2.34f);
        confettiParticleObject.GetComponent<ParticleSystem>().Play();
    }

    public void CreateRotatingObject()
    {
        DestroyExistingRotatingObject();

        GameObject item = FindObjectOfType<MagnetCollisionController>().LastItem.gameObject;
        currentRotatingObject = Instantiate(item, rotatingPrefabPos.position, Quaternion.identity);
        currentRotatingObject.AddComponent<ItemRotator>().ConfigureRotator(RotationAxis.Y);
    }

    public void DestroyExistingRotatingObject()
    {
        if (currentRotatingObject != null)
        {
            Destroy(currentRotatingObject);
        }
    }
}
