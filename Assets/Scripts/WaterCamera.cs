using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaterCamera : MonoBehaviour
{
    [SerializeField] Image waterMask;

    private void Start()
    {
        waterMask.gameObject.SetActive(false);
    }
    void Update()
    {
        if(gameObject.transform.position.y <= 1.5)
        {
            waterMask.gameObject.SetActive(true);
        }
        else
        {
            waterMask.gameObject.SetActive(false);
        }
    }
}
