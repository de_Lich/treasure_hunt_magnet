using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamemodeManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> finishScreenModeObjects = null;
    [SerializeField] private List<GameObject> startScreenModeObjects = null;
    [SerializeField] private List<GameObject> receiveScreenModeObjects = null;

    private List<GameObject> levelModeObjects = new List<GameObject>();

    public delegate void OnStartViewAppliedEventHandler(/*List<GameObject> _levelModeObjects*/);
    public event OnStartViewAppliedEventHandler OnStartViewApplied;

    #region Singleton Init
    private static GamemodeManager _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static GamemodeManager Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<GamemodeManager>();
        //_instance.Initialize();
    }
    #endregion

    public void EnableStartScreenMode()
    {
        ResetLevelModeObjects();
        LevelManager.Instance.CreateLevel(1, true);
        OnStartViewApplied?.Invoke();

        AnalyticsUtils.Instance.SubmitProgressionStartEvent("Default_Level");

        ToggleFinishScreenObjects(false);
        ToggleReceiveObjects(false);
        ToggleStartScreenObjects(true);
        ToggleLevelObjects(true);

        VFX_Controller.Instance.DestroyExistingRotatingObject();
        CameraMover.Instance.MoveToThrowView();
    }

    public void EnableFinishScreenMode()
    {
        MainData.Instance.gameState = MainData.GameState.HoldInput;
        CoroutineActions.ExecuteAction(1.5f, () => UI_Controller.Instance.ToggleResetPanel(true));

        ToggleStartScreenObjects(false);
        ToggleReceiveObjects(false);
        ToggleFinishScreenObjects(true);

        CameraMover.Instance.MoveToFinishLevelView();
    }

    public void EnableReceiveScreenMode()
    {
        AnalyticsUtils.Instance.SubmitProgressionSuccessEvent("Default_Level");

        ToggleReceiveObjects(true);
        ToggleLevelObjects(false);

        CameraMover.Instance.MoveToReceiveView();
    }

    public void RegisterLevelModeObject(GameObject go)
    {
        levelModeObjects.Add(go);
    }

    private void ResetLevelModeObjects()
    {
        levelModeObjects.Clear();
    }

    private void ToggleStartScreenObjects(bool value)
    {
        for (int i = 0; i < startScreenModeObjects.Count; i++)
        {
            startScreenModeObjects[i].SetActive(value);
        }
    }

    private void ToggleFinishScreenObjects(bool value)
    {
        for (int i = 0; i < finishScreenModeObjects.Count; i++)
        {
            finishScreenModeObjects[i].SetActive(value);
        }
    }

    private void ToggleLevelObjects(bool value)
    {
        for (int i = 0; i < levelModeObjects.Count; i++)
        {
            levelModeObjects[i].SetActive(value);
        }
    }

    private void ToggleReceiveObjects(bool value)
    {
        for (int i = 0; i < receiveScreenModeObjects.Count; i++)
        {
            receiveScreenModeObjects[i].SetActive(value);
        }
    }
}
