using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollisionManager : MonoBehaviour
{
    [SerializeField] private GameObjectStorage goStorage = null;

    private GameObject previewItemObj;
    private GameObject actualItemObj;

    private Item item = null;
    public Item Item
    {
        get
        {
            if (item == null)
            {
                item = actualItemObj.GetComponent<Item>();
            }

            return item;
        }
    }

    private void Awake()
    {
        GameObject previewItem = goStorage.GetElement();
        previewItemObj = Instantiate(previewItem, transform);
        actualItemObj = Instantiate(goStorage.GetElement(previewItem), transform);
    }

    public void Initialize(float itemMultiplier)
    {
        item.ClickMultiplier = itemMultiplier;
        actualItemObj.SetActive(false);
    }

    public void Collide(Transform magnetTransform)
    {
        previewItemObj.SetActive(false);
        actualItemObj.SetActive(true);

        Item.Hook(magnetTransform);
        UI_Controller.Instance.ToggleClickbar(true, 10 * Item.ClickMultiplier);

        GameObject cameraPoint = new GameObject("Camera Point");
        cameraPoint.transform.SetParent(actualItemObj.transform);
        cameraPoint.transform.localPosition = new Vector3(0, 3.5f, -2.5f);
        cameraPoint.transform.localRotation = Quaternion.Euler(40, 0, 0);
        CameraMover.Instance.SetAndMoveToTarget(cameraPoint.transform, false, 7);
    }
}
