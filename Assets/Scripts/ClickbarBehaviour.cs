using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClickbarBehaviour : MonoBehaviour
{
    [SerializeField] private Image barImage = null;
    [SerializeField] private float defaultProgressAmount = 30f;
    [SerializeField] private float progressDecreaseRate = 15;

    [NaughtyAttributes.ShowNonSerializedField] private float addedProgressAmount;
    private float progressAmount;
    
    private void Awake()
    {
        progressAmount = defaultProgressAmount;
    }

    private void Update()
    {
        progressAmount -= progressDecreaseRate * Time.deltaTime;
        progressAmount = Mathf.Clamp(progressAmount, 0f, 100f);
        barImage.fillAmount = GetManaNormalized();

        if(progressAmount > 95)
        {
            ReceiveItem();
            progressAmount = defaultProgressAmount;
        }
        if(progressAmount <= 0)
        {
            LoseItem();
            progressAmount = defaultProgressAmount;
        }
    }

    private void ReceiveItem()
    {
        UI_Controller.Instance.ToggleTradePanel(true);
        UI_Controller.Instance.ToggleClickbar(false);
        GamemodeManager.Instance.EnableReceiveScreenMode();
        VFX_Controller.Instance.CreateRotatingObject();
        //MainData.Instance.gameState = MainData.GameState.HoldInput;
    }

    private void LoseItem()
    {
        UI_Controller.Instance.ToggleClickbar(false);
        UI_Controller.Instance.ToggleText(3f, UI_TextType.ItemLost);
        FindObjectOfType<MagnetMovementController>().ResetState();
        AnalyticsUtils.Instance.SubmitProgressionFailEvent("Default_Level");
    }

    public void SetAddedProgressAmount(float value)
    {
        addedProgressAmount = value;
    }

    public void AddProgress()
    {
        progressAmount += addedProgressAmount;
    }

    private float GetManaNormalized()
    {
        return progressAmount / 100;
    }
}
