using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    #region Singleton Init

    private static GameController _instance;
    private static bool isInitialized = false; // A bit faster singleton

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static GameController Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<GameController>();
        if (_instance != null)
        {
            //_instance.Initialize();
            isInitialized = true;
        }
    }
    #endregion

    public event System.Action OnGameSessionStarted;

    public void StartGameSession()
    {
        OnGameSessionStarted?.Invoke();
    }
}
