using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemData
{
    [SerializeField] private float itemClickDifficultMultiplier = 0f;

    public void SetClickMultiplier(float value)
    {
        itemClickDifficultMultiplier = value;
    }
}
