using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemInitializer : MonoBehaviour
{
    [SerializeField] private float difficultyCoefficient = 0.2f;
    [SerializeField] private float difficultyOffset = 2f;
    [SerializeField] private float minClampValue = 1.5f;
    [SerializeField] private float maxClampValue = 5f;

    private List<ItemCollisionManager> items = new List<ItemCollisionManager>();

    private void OnEnable()
    {
        GamemodeManager.Instance.OnStartViewApplied += InitializeItems;
    }

    private void OnDisable()
    {
        GamemodeManager.Instance.OnStartViewApplied -= InitializeItems;
    }

    private void InitializeItems()
    {
        GetComponentsInChildren(items);
        EvaluateItemDifficulty();
        RegisterItems();
    }

    private void RegisterItems()
    {
        for (int i = 0; i < items.Count; i++)
        {
            GamemodeManager.Instance.RegisterLevelModeObject(items[i].gameObject);
        }
    }

    private void EvaluateItemDifficulty()
    {
        float zBound = PlayableAreaController.Instance.MaxBoundsVector.z;

        for (int i = 0; i < items.Count; i++)
        {
            float dZ = zBound - items[i].Item.transform.position.z;
            items[i].Initialize(Mathf.Clamp(dZ * difficultyCoefficient - difficultyOffset, minClampValue, maxClampValue));
        }
    }
}
