using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayableAreaController : MonoBehaviour
{
    #region Singleton Init

    private static PlayableAreaController _instance;
    private static bool isInitialized = false; // A bit faster singleton

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static PlayableAreaController Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<PlayableAreaController>();
        if (_instance != null)
        {
            //_instance.Initialize();
            isInitialized = true;
        }
    }
    #endregion

    [SerializeField] private float xBound = 20f;
    [SerializeField] private float zBound = 20f;
    [SerializeField] private Vector3 offset = default;
    [SerializeField] private bool debugMode = false;


/*    public float XBound => xBound;
    public float ZBound => zBound;
*/
    public Vector3 MaxBoundsVector => new Vector3(xBound, 0, zBound) + offset;
    public Vector3 MinBoundsVector =>/* new Vector3(xBound, 0, zBound) -*/ offset;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if (debugMode)
        {
            Gizmos.DrawLine(new Vector3(xBound, 0, zBound) + offset, new Vector3(-xBound, 0, zBound) + offset);
            Gizmos.DrawLine(new Vector3(-xBound, 0, zBound) + offset, new Vector3(-xBound, 0, -zBound) + offset);
            Gizmos.DrawLine(new Vector3(-xBound, 0, -zBound) + offset, new Vector3(xBound, 0, -zBound) + offset);
            Gizmos.DrawLine(new Vector3(xBound, 0, -zBound) + offset, new Vector3(xBound, 0, zBound) + offset);
        }
    }
}
