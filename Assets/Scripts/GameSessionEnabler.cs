using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSessionEnabler : MonoBehaviour
{
    private void Start()
    {
        GameController.Instance.OnGameSessionStarted += () => gameObject.SetActive(true);
        gameObject.SetActive(false);
    }
}
