using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnetCollisionController : MonoBehaviour
{
    private bool hasItem = false;
    public Item LastItem { get; private set; }

    //public delegate void OnCollisionEventHandler(bool isGood);
    public event System.Action OnItemCaught;

    private void OnTriggerEnter(Collider other)
    {
        if (!hasItem)
        {
            ItemCollisionManager itemCollisionManager = other.GetComponent<ItemCollisionManager>();
            if (itemCollisionManager != null)
            {
                itemCollisionManager.Collide(transform);
                OnItemCaught?.Invoke(/*itemCollisionManager.Item.ClickMultiplier * 0.1f, itemCollisionManager.Item.IsGood*/);

                LastItem = itemCollisionManager.Item;
                hasItem = true;
            }
        }

        MeshFilter meshFilter = other.GetComponent<MeshFilter>(); // Collisions should implement an interface to work in accordance to SOLID
        if (meshFilter != null)
        {
            VFX_Controller.Instance.PlaySplashEffect(transform.position);
            MainData.Instance.gameState = MainData.GameState.PlayingGame;
        }
    }

    public void ResetCollisionData()
    {
        if (LastItem != null)
        {
            LastItem.gameObject.SetActive(false);
        }

        hasItem = false;
    }
}
