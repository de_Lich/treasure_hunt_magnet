using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Game Object Storage", menuName = "ScriptableObjects/Game Object Storage")]
public class GameObjectStorage : ScriptableObject
{
    [SerializeField] private List<StorageElement> elements = null;

    public GameObject GetElement(GameObject key)
    {
        for (int i = 0; i < elements.Count; i++)
        {
            if (elements[i].IsKey(key))
            {
                return elements[i].StorageValues.GetRandomElement();
            }
        }

        Debug.LogError("Didn't find a matching key for " + key.name);
        return null;
    }

    public GameObject GetElement()
    {
        return elements.GetRandomElement().StorageValues.GetRandomElement();
    }
}
