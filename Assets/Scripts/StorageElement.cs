using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Storage Element", menuName = "ScriptableObjects/Game Object Storage Element")]
public class StorageElement : ScriptableObject
{
    [SerializeField] private GameObject storageKey = null;
    [SerializeField] private List<GameObject> storageValues = null;

    public GameObject StorageKey => storageKey;
    public List<GameObject> StorageValues => storageValues;

    public bool IsKey(GameObject gameObject) => gameObject == storageKey;
    
}
