using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rope : MonoBehaviour
{
    [SerializeField] private GameObject ropePart = null;
    [SerializeField] private Transform parent = null;

    //public List<Transform> targetPositions;

    [SerializeField] private Transform origin = null;
    [SerializeField] private Transform destination = null;
    [SerializeField] private int bitsNumber = 5;
    [SerializeField] private AnimationCurve curve = null;

    private List<GameObject> ropeBits = new List<GameObject>();

/*    public Transform testStart;
    public Transform testEnd;
    private GameObject testRopes;*/

    private void Start()
    {
        GameController.Instance.OnGameSessionStarted += GenerateRope;
    }

    private void LateUpdate()
    {
        UpdateRope();
    }

    private void UpdateRope()
    {
        Vector3 delta = destination.position - origin.position;
        Vector3 step = delta / bitsNumber;

        Vector3 currentPosition = origin.position;
        //Vector3 nextPosition = origin.position + step;
        Vector3 nextPosition = origin.position + new Vector3(step.x, step.y * curve.Evaluate(1 / bitsNumber), step.z);

        for (int i = 0; i < ropeBits.Count; i++)
        {
            SetRopePosition(currentPosition, ropeBits[i].transform);
            SetRopeRotation(nextPosition, ropeBits[i].transform);
            SetRopeLength(nextPosition, ropeBits[i].transform);

            currentPosition = nextPosition;
            //nextPosition = nextPosition + step;  
            nextPosition += new Vector3(step.x, step.y * curve.Evaluate((i + 1) / (float)bitsNumber), step.z);
        }
    }

    private void GenerateRope()
    {
        Vector3 delta = destination.position - origin.position;
        Vector3 step = delta / bitsNumber;

        Vector3 currentPosition = origin.position;
        //Vector3 nextPosition = origin.position + step;
        Vector3 nextPosition = origin.position + new Vector3(step.x, step.y * curve.Evaluate(1 / bitsNumber), step.z);

        for (int i = 0; i < bitsNumber; i++)
        {
            GameObject ropeBit = SpawnRopeRet(currentPosition, nextPosition);
            ropeBits.Add(ropeBit);
            currentPosition = nextPosition;
            //nextPosition = nextPosition  + step;
            nextPosition += new Vector3(step.x, step.y * curve.Evaluate((i + 1) / (float)bitsNumber), step.z);
        }
    }

/*    [NaughtyAttributes.Button]
    private void Test()
    {
        ClearExistingRopes();
        testRopes = new GameObject("Test_Ropes");
        Vector3 delta = testEnd.position - testStart.position;
        Vector3 step = delta / bitsNumber;

        Vector3 currentPosition = testStart.position;
        //Vector3 nextPosition = testStart.position + step;
        Vector3 nextPosition = testStart.position + new Vector3(step.x, step.y * curve.Evaluate(1 / bitsNumber), step.z);
        //nextPosition.y *= curve.Evaluate(1 / (float)bitsNumber);

        for (int i = 0; i < bitsNumber; i++)
        {
            float a = i / (float)bitsNumber;
            float b = (i + 1) / (float)bitsNumber;

            SpawnRope(currentPosition, nextPosition);
            currentPosition = nextPosition;
            //nextPosition = nextPosition + step;
            nextPosition = nextPosition + new Vector3(step.x, step.y * curve.Evaluate(b), step.z);
            ///nextPosition.y *= curve.Evaluate(b);
        }
    }*/


/*    [NaughtyAttributes.Button]
    private void SpawnRopes()
    {
        for (int i = 0; i < targetPositions.Count - 1; i++)
        {
            SpawnRope(targetPositions[i].position, targetPositions[i + 1].position);
        }
    }*/

/*    [NaughtyAttributes.Button]
    private void ClearExistingRopes()
    {
        DestroyImmediate(testRopes);
    }*/
/*    private void ClearExistingRopes()
    {
        int childCount = parent.childCount;

        for (int i = 0; i < childCount; i++)
        {
            Destroy(parent.GetChild(i).gameObject);
        }
    }*/

/*    private void SpawnRope(Vector3 targetPosition, Vector3 endPosition)
    {
        GameObject go = Instantiate(ropePart, targetPosition, Quaternion.identity, testRopes.transform);
        SetRopeRotation(endPosition, go.transform);
        SetRopeLength(endPosition, go.transform);
    }*/

    private GameObject SpawnRopeRet(Vector3 targetPosition, Vector3 endPosition)
    {
        GameObject go = Instantiate(ropePart, targetPosition, Quaternion.identity, parent);
        SetRopeRotation(endPosition, go.transform);
        SetRopeLength(endPosition, go.transform);
        return go;
    }

    private void SetRopePosition(Vector3 targetPosition, Transform targetRope)
    {
        targetRope.position = targetPosition;
    }

    private void SetRopeRotation(Vector3 targetPosition, Transform targetRope)
    {
        targetRope.rotation = Quaternion.LookRotation(targetPosition - targetRope.position, Vector3.up);
    }

    private void SetRopeLength(Vector3 targetPosition, Transform targetRope)
    {
        float length = (targetPosition - targetRope.position).magnitude;
        targetRope.localScale = new Vector3(1, 1, length);
    }


}
