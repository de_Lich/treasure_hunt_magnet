using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;

public class AnalyticsUtils : MonoBehaviour
{
    #region Singleton Init
    private static AnalyticsUtils _instance;

    void Awake() // Init in order
    {
        if (_instance == null)
            Init();
        else if (_instance != this)
            Destroy(gameObject);
    }

    public static AnalyticsUtils Instance // Init not in order
    {
        get
        {
            if (_instance == null)
                Init();
            return _instance;
        }
        private set { _instance = value; }
    }

    static void Init() // Init script
    {
        _instance = FindObjectOfType<AnalyticsUtils>();
        //_instance.Initialize();
    }
    #endregion

    private void Start()
    {
        GameAnalytics.Initialize();
    }

    public void SubmitProgressionStartEvent(string levelName)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, levelName);
    }

    public void SubmitProgressionFailEvent(string levelName)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Fail, levelName);
    }

    public void SubmitProgressionSuccessEvent(string levelName)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, levelName);
    }

}
