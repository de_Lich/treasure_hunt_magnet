using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class THM_Utils
{
    public static T GetRandomElement<T>(this List<T> elements)
    {
        return elements[Random.Range(0, elements.Count)];
    }
}
