using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private Vector3 offset = new Vector3(0, 0, 1);
    [SerializeField] private bool isGood = false;

    private bool isBeingReeled = false;
    private Transform magnetTransform;
    
    public float ClickMultiplier { get; set; }
    public bool IsGood => isGood;
    public Vector3 Offset => offset;

    private void Update()
    {
        if (isBeingReeled)
        {
            transform.position = magnetTransform.position + offset;
        }
    }

    public void Hook(Transform _magnetTransform)
    {
        isBeingReeled = true;
        magnetTransform = _magnetTransform;
    }
}
